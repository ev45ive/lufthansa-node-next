

import React from 'react'

export const Placki = ({ posts }) => {
    return (
        <div style={{ margin: '30px' }}>
            <h1>Placki</h1>

            <ul>
                {posts.map(post => <li key={post.id}>{post.title}</li>)}
            </ul>
        </div>
    )
}

export default Placki

// This function gets called at build time
export async function getStaticProps() {
    // Call an external API endpoint to get posts
    // const res = await fetch('https://jsonplaceholder.typicode.com/posts')
    // json-server --port 6000 https://jsonplaceholder.typicode.com/db
    const res = await fetch('http://localhost:6000/posts')
    const posts = await res.json()

    // By returning { props: { posts } }, the Blog component
    // will receive `posts` as a prop at build time
    return {
        props: {
            posts,
        },
    }
}