import express from 'express'
import { plackiRoutes } from './routes/placki';
// import 'socket.io'

const app = express()

app.use(express.static('/public'))

app.get('/', (req, res) => {
    const name = req.query.name || 'Guest';

    res.send(`<h1>Hello ${name}</h1>`)
})

app.use('/api/placki', plackiRoutes)

app.listen(8080, () => {
    console.log('Server listening on http://localhost:8080/ ');
})