import { Router } from "express";

export const plackiRoutes = Router()


const desserts = [
    { name: 'placki' },
    { name: 'ciastka' },
    { name: 'ciastka z kremem' },
]

plackiRoutes.get('/', (req, res) => {
    const filter = String(req.query.filter || '')

    const results = desserts.filter(d => d.name.includes(filter))

    res.json(results)
})